def min(t1, t2) {
	if(t1.compareTo(t2) < 0 ) {
		t1
	} else {
		t2
	}
}

def max(t1, t2) {
	if(t1.compareTo(t2) > 0 ) {
		t1
	} else {
		t2
	}
}

def nbrRange() {
	self.routingDistance(nbr(self.getDevicePosition()))
}

def nbrRangeFun() {
	let range = nbrRange();
	mux(range > 1) {
		range
	} else {
		1000
	}
}

def gBB(source, initial, metric, accumulate) {
	rep(distanceValue <- [Infinity, initial]) {
		mux(source) {
			[0, initial]
		} else {
			let ndv = nbr(distanceValue);
			minHood([
				ndv.get(0) + metric.apply(),
				accumulate.apply(ndv.get(1))
			])
		}
	}.get(1)
}

def distanceTo(source) {
	gBB(source, 0, nbrRangeFun, (v) -> {v + nbrRangeFun()})
}

def tBB(initial, zero, decay) {
	rep(v <- initial) {
		min(initial, max(zero, decay.apply(v)))
	}
}

def limitedMemory(value, timeout) {
	tBB([timeout, value], [0, false], (t) -> {[t.get(0) - self.getDeltaTime(), t.get(1)]}).get(1)
}

def rtSub(started, state, memoryTime) {
	if(state) {
		true
	} else {
		limitedMemory(started, memoryTime)
	}
}

def timer(length) {
	tBB(length, 0, (t) -> {t - self.getDeltaTime()})
}

def recentlyTrue(state, memoryTime) {
	rtSub(timer(10) == 0, state, memoryTime)
}

def findParent(potential) {
	mux(minHood(nbr(potential)) < potential) {
		let closest = minHood(nbr([potential, self]));
		if(closest.size()>1) {
			closest.get(1)
		} else {
			-1
		}
	} else {
		NaN
	}
}

def cBB(potential, accumulate, local, null) {
	rep(v <- local) {
		accumulate.apply(local,
			/*
			 * TODO: switch to accumulateHood
			 */
			sumHood(
				mux(nbr(findParent(potential)) == self) {
					nbr(v)
				} else {
					null
				}
			)
		)
	}
}

def identity(v) {
	v
}

def broadcast(source, value) {
	gBB(source, value, nbrRangeFun, identity)
}

def summarize (sink, accumulate, local, null) {
	broadcast(sink, cBB(distanceTo(sink), accumulate, local, null))
}

def sum(a, b) {
	a + b
}

def average(sink, value) {
	summarize(sink, sum, value, 0) / summarize(sink, sum, 1, 0)
}

/*
 * In FOCAS avg = 2.17, sum = 300
 */
def dangerousDensity(partition, p, commRange, avgThreshold, sumThreshold) {
	if (average(partition, densityEst(p, commRange)) > avgThreshold && summarize(partition, sum, 1 / p, 0) > sumThreshold) {
		2
	} else {
		1
	}
}

def distanceCompetition(d, lead, uid, grain, metric) {
	mux(d > grain * 1.5) {
		uid
	} else {
		let gsize = grain * 0.5;
		mux (d >= gsize) {
			Infinity
		} else {
			minHood(
				mux(nbr(d) + metric.apply() >= gsize) {
					Infinity
				} else {
					nbr(lead)
				}
			)
		}
	}
}

def breakUsingUIDs(uid, grain, metric) {
	uid == rep(lead <- uid) {
		distanceCompetition(
			gBB(uid == lead, 0, metric, (v) -> {v + metric.apply()}),
			lead, uid, grain, metric)
	}
}

def randomUID() {
	getId()/1480;
}

def getId() {
	self.getDeviceUID().getId()
}

def sBB(grain, metric) {
	breakUsingUIDs(randomUID(), grain, metric)
}

def densityEst(p, range) {
 let nearby = unionHood PlusSelf(
 	mux (nbrRange() < range) {
 		nbr([getId()])
 	} else {
 		[]
 	}
 );

 let footprint = 0.25;
 nearby.size() / p / (pi * range ^ 2 * footprint)
}

public def crowdTracking(p, range, commRange, avgThreshold, sumThreshold, maxDensity, timeFrame) {
	let res = if(recentlyTrue(densityEst(p, commRange) > maxDensity, timeFrame)) {
		dangerousDensity(sBB(range, nbrRangeFun), p, commRange, avgThreshold, sumThreshold)
	} else {
		0
	};
	env.put("tracking", res);
	res
}

public def crowdWarning(p, tRange, wRange, commRange, avgThreshold, sumThreshold, maxDensity, timeFrame) {
	distanceTo(crowdTracking(p, tRange, commRange, avgThreshold, sumThreshold, maxDensity, timeFrame) == 2) < wRange
}

let p = 0.005;
let range = 30;
let wRange = 60;
let	commRange = 30;
let	avgThreshold = 2.17;
let	sumThreshold = 300;
let	maxDensity = 1.08;
let	timeFrame = 60;
let res = crowdWarning(p, range, wRange, commRange, avgThreshold, sumThreshold, maxDensity, timeFrame);
env.put("warning", res);
res