# README

This repository contains all the tools for reproducing the results of the paper: *Combining Self-Organisation and Autonomic Computing in CASs with Aggregate-MAPE* by Mirko Viroli, Antonio Bucchiarone, Danilo Pianini and Jacob Beal.

## Requirements

This software requires the Java 8 Development Kit to be installed on your system. To verify, open a terminal and type:

``java -version``

The expected output lines should include the name of your Java provider (e.g. `OpenJDK Runtime Environment`) and the version, e.g. `build 1.8.0_92-b14`. The latter must be `1.8` or above. Failing in matching this requirement will prevent the tests from compiling.

## Configure the batch

As first step, you should edit the `gradle.properties` file setting an appropriate amount of maximum memory. The batch whose results are presented in the paper comlpleted on a eight-core Intel Xeon machine with 16GB RAM. The batch took about a week. The batch should be able to complete flawlessly as far as at least 2GB of RAM per (logic) core are available.

## Run the simulation batch

To re-execute the whole batch (ten runs for the control plus ten runs for the experiment), pull up a terminal and run:

``./gradlew``

If you are a Windows user, you can try to use the provided batch file:

``gradlew.bat``

Please be wary that we have not tested the system under such environment.

Please note: re-running the simulations can easily take days on a well equipped machine. At the beginning of the simulation, there will be some error printed by Reflections. Those are not errors you should be scared of. However, make sure that after those no other error occurs (in the Alchemist initializator, for instance).

## Regenerating charts from simulation data

A Python script is included with the repository for generating charts. You will need to install Python 3, NumPy and MatplotLib yourself. Once done, and supposing that you can invoke your Python 3 interpreter with the `python` command, just open a terminal in the directory you cloned and issue:

``python process.py``

## Running personalised simulations

Take a look at `build.gradle`, at the YAML Alchemist simulation files and at the Protelis code. Don't forget to read the [Alchemist tutorial](https://alchemistsimulator.github.io/pages/tutorial/) and the [Alchemist tutorial for using Protelis in conjunction with the simulator](https://github.com/AlchemistSimulator/Protelis-Incarnation-tutorial).
