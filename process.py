# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: danysk
"""

#import scipy as scp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmx
import fnmatch
import os
import math
import re
import operator
from itertools import groupby

#a = np.linspace(0, 100, num = 10000)
#plt.plot(a, np.sin(a))
#print(a)

def getClosest(val, sortedMatrix):
#    return min(matrix, key=lambda row: abs(row[0]-val))[1]
    l = len(sortedMatrix)
    if l == 1:
        return [val] + sortedMatrix[1 :]
    if l == 2:
#        if abs(sortedMatrix[0][0] - val
        return [val] + min(sortedMatrix, key=lambda row: abs(row[0]-val))[1:]
    half = int(l / 2)
    if(sortedMatrix[half][0] < val):
        return getClosest(val, sortedMatrix[-half - 1:]) #last half matrix
    return getClosest(val, sortedMatrix[: half + 1]) # first half matrix
        

def convert(range, matrix):
    return [getClosest(t, matrix) for t in range]

def convFloat(x, limit='inf'):
    try:
        result = float(x)
        if result == float('inf') or result == float('-inf') or result > limit:
            return float('NaN')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x, limit = 10e6) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

def getVarValue(var, target):
    match = re.search('(?<=' + var +'-)\d+(\.\d*)?', target)
    return match.group(0)

def collectData(testvar, base = None, dir=".", ext="txt"):
    if base == None:
        base = testvar
#    return list(
#        map(openCsv,
#            filter(lambda file: fnmatch.fnmatch(file, base + '_' + '*' + testvar + '-*.' + ext), os.listdir(dir))
#        )
#    )
    allfiles = set(filter(lambda file: fnmatch.fnmatch(file, base + '_' + '*' + testvar + '-*.' + ext), os.listdir(dir)))
#    allvalues = list(map(lambda name: getVarValue(testvarm name)))
#    print("files = " + list(allfiles))
    couples = map(lambda x: (getVarValue(testvar, x), openCsv(dir + '/' + x)), allfiles) #list of (varvalue, fileContent) tuples
    couples = sorted(couples)
    grouped = groupby(couples, lambda x: x[0])
#    groups = []
#    uniquekeys = []
#    for k, g in grouped:
#        groups.append(list(g))      # Store group iterator as a list
 #       uniquekeys.append(k)
    return dict([(k, list(v[1] for v in g)) for k, g in grouped])

# CONFIGURE SCRIPT
np.set_printoptions(formatter={'float': '{: 0.1f}'.format})
variables = ["mape"]
experiments = ['raw']
sampling = 1
refColumn = 0
maxTime = 10800
minTime = 0
timeScale = 1 / 60

titlesize = 14
axissize = 12
legendsize = 10
colormap = plt.get_cmap('nipy_spectral')
linewidth = 2
figure_size=(8, 4)
crunchData = True

# CRUNCH DATA
if crunchData:
    for experiment in experiments:
        for var in variables:
            # Key : 3D-matrix
            data = {int(float(key)): matrices for key, matrices in collectData(var, base = experiment, dir = 'data').items()}
            samples = np.linspace(minTime, maxTime, num = math.ceil((maxTime - minTime) / sampling) + 1)
            # Match the samples
            sampled = {key : list(map(lambda matrices: convert(samples, matrices), matrices)) for key, matrices in data.items()}
            # Mean of the matrices
            summarized = {key : np.matrix(np.nanmean(matrices, axis = 0)) for key, matrices in sampled.items()}
            errors = {key : np.matrix(np.nanstd(matrices, axis = 0)) for key, matrices in sampled.items()}
            # sort
            sortedData = sorted(summarized.items(), key = operator.itemgetter(0))
            sortedErr = sorted(errors.items(), key = operator.itemgetter(0))

# EXTRACT DATA TO BE PLOTTED
time = samples * timeScale
nomape = sortedData[0][1]
mape = sortedData[1][1]
allDevices = np.asarray(nomape[:,1])
danger = np.asarray(nomape[:,2])
warning = np.asarray(nomape[:,3])
mapeDanger = np.asarray(mape[:,2])
mapeWarning = np.asarray(mape[:,3])
mapeDisperse = np.asarray(mape[:,4])
mapeStill = np.asarray(mape[:,5]) - np.asarray(nomape[:,5])# * mapeDisperse / allDevices

# PLOT

ylabel = 'Number of devices'
xlabel = 'Time (simulated minutes)'

fig_dispersal = plt.figure(figsize=figure_size);
plot_dispersal = fig_dispersal.add_subplot(1, 1, 1)
plot_dispersal.set_title('Dispersal plan efficacy', fontsize=titlesize)
plot_dispersal.set_xlabel(xlabel, fontsize=axissize)
plot_dispersal.set_ylabel(ylabel, fontsize=axissize)
plot_dispersal.tick_params(axis='both', which='major', labelsize=int(axissize*0.9))
plot_dispersal.grid(b = True, which = 'both')
plot_dispersal.plot(time, danger, label='Danger perceived (M) without dispersal', color='orange')
plot_dispersal.plot(time, warning, label='Warning area (A) without dispersal', color='black')
plot_dispersal.plot(time, mapeDanger, label='Danger perceived (M) with dispersal', color='red')
plot_dispersal.plot(time, mapeWarning, label='Warning area (A) with dispersal', color='blue')
plot_dispersal.legend(fontsize = legendsize, loc = 'upper left',
      #bbox_to_anchor=(0.5, 1),
      ncol=2)
plot_dispersal.set_ylim([0, 495])
plot_dispersal.set_xlim([minTime * timeScale, maxTime * timeScale])

fig_involve = plt.figure(figsize=figure_size);
plot_involve = fig_involve.add_subplot(1, 1, 1)
plot_involve.set_title('Dispersal involvement', fontsize=titlesize)
plot_involve.set_xlabel(xlabel, fontsize=axissize)
plot_involve.set_ylabel(ylabel, fontsize=axissize)
plot_involve.tick_params(axis='both', which='major', labelsize=int(axissize*0.9))
plot_involve.grid(b = True, which = 'both')
nomape = sortedData[0][1]
mape = sortedData[1][1]
plot_involve.plot(time, mapeDisperse, label='Devices where dispersal is selected (P)', color='blue')
plot_involve.plot(time, mapeStill, label='Devices where stand still is selected (P)', color='green')
plot_involve.plot(time, mapeDanger, label='Danger perceived (M)', color='red')
plot_involve.plot(time, mapeWarning, label='Warning area (A)', color='orange')
plot_involve.legend(fontsize = legendsize, loc = 'upper center',
      #bbox_to_anchor=(0.5, 1),
      ncol=2)
plot_involve.set_ylim([0, 610])
plot_involve.set_xlim([minTime * timeScale, maxTime * timeScale])

fig_involve.tight_layout()
fig_dispersal.tight_layout()
fig_involve.savefig('involvement.pdf')
fig_dispersal.savefig('dispersal.pdf')


















